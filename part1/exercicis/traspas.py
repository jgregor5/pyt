
"""
Crea un petit programa en Python que llegeixi un any i en una variable booleana que es digui deTraspas es guardi el resultat d'avaluar si l'any llegit és o no de traspàs.
Recorda, un any és de traspàs si es compleixen alguna de les següents condicions:
    - Sí el número és múltiple de 4 i no és múltiple de 100.
    - Si el número és múltiple de 400.
Nota: Un número n1 és múltiple d'altre número n2 si el residu de dividir n1 entre n2 dóna zero

Exemples:
2004, 2008, 2012, 2016, 2020, 2024, 2028, 2032, 2036, 2040, 2044, 2048, 2052, 2056, 2060, 2064, 
2068, 2072, 2076, 2080, 2084, 2088, 2092, 2096. 
"""

any = int(input("any? "))
multiple4 = any % 4 == 0
noMultiple100 = not(any % 100 == 0)
multiple400 = any % 400 == 0
deTraspas = multiple4 and noMultiple100 or multiple400
print(f"{any} de traspàs: {deTraspas}")
