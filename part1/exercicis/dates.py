
SEGONS_1_DIA = 60*60*24
SEGONS_1_HORA = 60*60
SEGONS_1_MINUT = 60

totsegons = int(input("segons? "))

dies = totsegons // SEGONS_1_DIA
resta1 = totsegons % SEGONS_1_DIA

hores = resta1 // SEGONS_1_HORA
resta2 = resta1 % SEGONS_1_HORA

minuts = resta2 // SEGONS_1_MINUT
resta3 = resta2 % SEGONS_1_MINUT

print(f"{dies} dies, {hores} hores, {minuts} minuts i {resta3} segons")
