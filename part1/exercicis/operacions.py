a = "Pere"
b = "Pol"

# les longituds són iguals: False
print(len(a) == len(b))
print(not(len(a) != len(b)))
# totes tenen longitud 3: False
print(len(a) == 3 and len(b) == 3)
# alguna longitud és més petita de 4: True
print(len(a) < 4 or len(b) < 4)
# alguna acaba amb l: True
print(a[-1] == "l" or b[-1] == "l")
# totes comencen per P: True
print(a[0] == "P" and b[0] == "P")
# alguna no conté la r: True
print(a.find("r") == -1 or b.find("r") == -1)
# la longitud de a és major que la de b: True
print(len(a) > len(b))
# la longitud de a NO és major que la de b: False
print(not(len(a) > len(b)))
# la longitud d'a està entre 3 i 4 i la de b no és més petita que la d'a: False
print(len(a) >= 3 and len(a) <= 4 and not(len(b) < len(a)))
print(len(a) >= 3 and len(a) <= 4 and len(b) >= len(a))
