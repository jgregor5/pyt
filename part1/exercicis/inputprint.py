
# pregunta una paraula (per exemple: avió) i escriu per pantalla: 
# avió té 4 lletres
# de tres formes:
# 1) print amb un paràmetre i concatenació de strings
# 2) print amb diversos paràmetres separats per comes
# 3) f print

paraula = input("paraula? ")
print(paraula + " té " + str(len(paraula)) + " lletres")
print(paraula, "té", len(paraula), "lletres")
print(f"{paraula} té {len(paraula)} lletres")
