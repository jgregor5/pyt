
# Crea un programa que llegeix una quantitat per teclat i si és superior a 10.000 se li fa el 10% de descompte, en cas contrari únicament li fa el 5%. El resultat cal que ens digui la quantitat descomptada i la quantitat a pagar.

quantitat = int(input("quantitat? "))

if quantitat > 10000:
    descompte = 10
else:
    descompte = 5

descomptat = quantitat * descompte/100
apagar = quantitat - descomptat

print(f"descompte del {descompte}%, descomptat {descomptat}, a pagar {apagar}")
