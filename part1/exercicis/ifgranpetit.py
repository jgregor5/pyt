
# Crea un programa que llegeix 3 números i digui quin és el més gran i quin el més petit.

num1 = int(input("número 1? "))
num2 = int(input("número 2? "))
num3 = int(input("número 3? "))

if num1 >= num2 and num1 >= num3:
    gran = num1
elif num2 >= num1 and num2 >= num3:
    gran = num2
else:
    gran = num3

if num1 <= num2 and num1 <= num3:
    petit = num1
elif num2 <= num1 and num2 <= num3:
    petit = num2
else:
    petit = num3

print(f"el més petit és el {petit}, i el més gran el {gran}")
