str1 = input("1? ")
str2 = input("2? ")

str1m = str1.upper()
str2m = str2.upper()
str1s = str1.replace(" ", "")
str2s = str2.replace(" ", "")
str1sm = str1.upper().replace(" ", "")
str2sm = str2.upper().replace(" ", "")

if str1 == str2:
    print("exactament iguals")
else: 
    if str1m == str2m:
        print("iguals ignorant case")        
    elif str1s == str2s:
        print("iguals ignorant espais")        
    elif str1sm == str2sm:
        print("iguals ignorant case i espais")        
    else:
        print("diferents")

# asdf | asdf ==> exactament iguals
# asdf | ASDF ==> iguals ignorant case
# asdf | a s d f ==> iguals ignorant espais
# asdf | A S D F ==> iguals ignorant case i espais

