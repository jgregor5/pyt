
# Crea un programa que demani a l’usuari la nota de les 2 UFs del mòdul de programació. Després, ha de calcular la nota final del mòdul, tenint en compte el següent: UF1 val el 20%, UF2 val el 30% i UF3 val el 50%.  Després de calcular la nota final, ha de mostrar a l’usuari si està aprovat o no. En el cas que estigui aprovat, a més ha de mostrar la nota final. En el cas que estigui suspès, la nota final no es mostrarà.

uf1 = float(input("UF1 (0-10)? "))
uf2 = float(input("UF2 (0-10)? "))
uf3 = float(input("UF3 (0-10)? "))

nota = uf1*20/100 + uf2*30/100 + uf3*50/100
aprovat = nota >= 5

if aprovat:
    print(f"la teva nota és {nota}")
else:
    print("has suspés")
