# comencem amb Python!
print("Hello, 'world' 1")
print() # una línia buida sense contingut
print('Hello, "world" 2')
print("Hello, \"world\" 3")
print('Hello, \'world\' 4')
print("Primera part", end="-")
print("Segona part")
print("U\nDos\nTres")
print("U\tDos\tTres")
print("Hola, món!")
