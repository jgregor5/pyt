import os
from datetime import datetime

if not os.path.exists("./data"):
  os.makedirs("./data")

writer_path = os.path.realpath('./data/writer.txt')

with open(writer_path, "a") as file:
  file.write(f"added at {datetime.now()}\n")
