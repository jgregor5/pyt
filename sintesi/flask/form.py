# importing Flask and other modules
from flask import Flask, request, render_template, render_template_string

# Flask constructor
app = Flask(__name__)

# A decorator used to tell the application
# which URL is associated function

@app.route('/', methods=["GET"])
def myroot():
  return render_template_string("<a href=\"{{ url_for('myform')}}\">Go to the form</a>")

@app.route('/form', methods=["GET", "POST"])
def myform():
  if request.method == "POST":
    # getting input with name = fname in HTML form
    first_name = request.form.get("fname")
    # getting input with name = lname in HTML form
    last_name = request.form.get("lname")
    return render_template("result.html", full_name = first_name + " " + last_name)
  # GET: mostra formulari
  return render_template("form.html")

if __name__ == '__main__':
  app.run()
