
FLASK
================================================================

pip install -r requirements.txt
(segurament només cal instal.lar Flask)

Pots provar dues apps flask:
python hello.py
Mostra una salutació en format JSON a l'adreça:
http://127.0.0.1:5000/hello

python form.py
Crea un formulari i mostra el resultat per pantalla:
http://127.0.0.1:5000/form

writer.py
================================================================

Aquest script crea un arxiu a la carpeta date (si no existeix) 
i afegeix un missatge de text al final amb un timestamp.
