x = 'hola, món!'

def f1():
  # global is not needed to read!
  print(f"from f1 x is {x}")

def f2():
  x = "hello, world!"
  print(f"from f2 x is {x}")

def f3():
  global x
  x = "¡hola, mundo!"
  print(f"from f3 x is {x}")

def f4():
  global x
  x = x + " fine"
  print(f"from f4 x is {x}")

def f5():
  y = x + " ok"
  print(f"from f5 y is {y}")
  # if a variable is assigned in a function, and is not marked global, then is local
  # therefore, you cannot read it before it is assigned
  x = y # error: local variable x referenced before assignment

f1()
print(f"after f1 is {x}")
f2()
print(f"after f2 is {x}")
f3()
print(f"after f3 is {x}")
f4()
print(f"after f4 is {x}")
f5()
