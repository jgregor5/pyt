
def f1(nombre):
  nombre = nombre + 1
  print(f"nombre dins és {nombre}")

def f2(cadena):
  cadena = cadena + " afegit"
  print(f"cadena dins és {cadena}")

def f3(llista):
  llista.append("món")
  print(f"llista dins és {llista}")

def f4(tupla):
  tupla = tupla + ("món",)
  print(f"tupla dins és {tupla}")

nombre = 123
f1(nombre)
print(f"nombre després és {nombre}")

cadena = "hola"
f2(cadena)
print(f"cadena després és {cadena}")

llista = ["hola"]
f3(llista)
print(f"llista després és {llista}")

tupla = ("hola",)
f4(tupla)
print(f"tupla després és {tupla}")