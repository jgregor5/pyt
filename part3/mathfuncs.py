
def suma(a, b):
  return a + b

def mult(a, b):
  return a * b

print(3*(2+3) + 2*4)
print(suma(mult(3, suma(2, 3)), mult(2, 4)))

def fibonacci(n):
  if n <= 0:
    return  
  elif n == 1:
    return [0]
  elif n == 2:
    return [0, 1]
  else:  
    l = [0, 1]
    for i in range(n - 2):
      l.append(l[-1] + l[-2])
    return l;

mida = int(input("mida? "))
sequencia = fibonacci(mida)
print(f"{len(sequencia)}: {sequencia}")
