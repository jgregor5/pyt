
def f1():
  print("Hola, món!")
  # returns None

def f2():
  return "Hola, món!"

def f3(nom):
  return f"Hola, {nom}!"

def f4(nom = "món"):
  return f"Hola, {nom}!"

def f5(nom, lang = 'ca'):
  msg = None
  if lang == 'ca':
    msg = f"Hola, {nom}!"
  elif lang == 'es':
    msg = f"¡Hola, {nom}!"
  elif lang == 'en':
    msg = f"Hello, {nom}!"
  return msg

print(f5('Pere', 'ca'))
print(f5('Pere', 'es'))
print(f5('Pere', 'en'))
print(f5('Pere', 'pt'))
