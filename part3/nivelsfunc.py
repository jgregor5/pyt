
def f1(a):
  return a + 1

def f2(a):
  return f1(a) * 2

def f3(a, b):
  return f2(a - 1) + f2(b + 1) + 3

print(f3(3, 4))


# f3(3, 4) =
# f2(2) + f2(5) + 3 =
# f1(2)*2 + f1(5)*2 + 3 = 
# 3*2 + 6*2 + 3 = 
# 6 + 12 + 3 = 21