# GLOBAL
# Afegeix les variables globals que necessitis per al teu programa

noms = []
nums = []

# FUNCIONS
# Afegeix les funcions que cridaràs en la zona MAIN

def mostrarMenu():
    print("\n1: Veure cistella")
    print("2: Afegir ítem")
    print("3: Treure ítem")
    print("4: Cercar ítem")
    print("5: Buidar cistella")
    print("6: Sortir")

def inputNumero(text, min, max):
    return int(input(f"{text}? "))

def inputText(text):
    return input(f"{text}? ")

def veureCistella():
    if len(noms) == 0:
        print("cistella buida")
    else:    
        for i in range(len(noms)):
            print(f"{nums[i]} {noms[i]}")

def cercarItem(nom):
    if nom in noms:
        posicio = noms.index(nom)
        return nums[posicio]
    else:
        return 0

def afegirItem(nom, num):
    if nom in noms:
        posicio = noms.index(nom)
        nums[posicio] = nums[posicio] + num
    else:
        noms.append(nom)
        nums.append(num)

def treureItem(nom, num):
    if nom in noms:
        posicio = noms.index(nom)
        if num == nums[posicio]:
            del noms[posicio]
            del nums[posicio]
            return True
        elif num < nums[posicio]:
            nums[posicio] = nums[posicio] - num
            return True
        else:
            return False
    else:
        return False


def buidarItems():
    global noms, nums
    noms = []
    nums = []
    print("cistella buidada")


# MAIN
# Bucle principal del programa que va cridant les funcions

def main():
    final = False
    while not final:
        mostrarMenu()
        opcio = inputNumero("opció", 1, 6)
        if opcio == 1:
            veureCistella()
        elif opcio == 2:
            nom = inputText("nom")
            num = inputNumero("quantitat", 1, 1000)
            afegirItem(nom, num)
        elif opcio == 3:
            nom = inputText("nom")
            num = inputNumero("quantitat", 1, 1000)        
            ok = treureItem(nom, num)
            if not ok:
                print("no es pot esborrar")
        elif opcio == 4:
            nom = inputText("nom")
            num = cercarItem(nom)
            print(f"hi ha {num}")
        elif opcio == 5:
            buidarItems()
        elif opcio == 6:
            final = True

if __name__ == "__main__":
  main()
