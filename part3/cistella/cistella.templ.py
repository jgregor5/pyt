# GLOBALS
# Afegeix les variables globals que necessitis per al teu programa


# FUNCIONS
# Afegeix les funcions que cridaràs en la zona MAIN

def veureCistella():
    # mostrar la llista d'ítems amb noms i quantitats
    # si no hi ha ítems, mostrar un missatge digui que no hi ha
    # no retorna res
    pass


def afegirItem(nom, num):
    # afegir una quantitat a un ítem, modificar-lo si ja existeix
    # nom és el nom de l'ítem a afegir, num la quantitat
    # no retorna res
    pass


def treureItem(nom, num):
    # nom és el nom de l'ítem a treure, num la quantitat
    # retornar True si s'ha pogut treure, False en cas contrari
    pass


def cercarItem(nom):
    # cercar un ítem per nom
    # retornar la quantitat actual, zero si no hi ha
    pass


def buidarItems():
    # buidar la llista d'ítems
    # no retorna res
    pass


def mostrarMenu():
    # mostrar la llista d'opcions
    # no retorna res
    pass


def inputText(text):
    # demana un string no buit
    # text és el missatge que convida a fer l'entrada
    # retorna el text introduït
    pass


def inputNumero(text, min, max):
    # demana un sencer entre min i max
    # text és el missatge que convida a fer l'entrada
    # si el nombre no està entre min i max, torna a preguntar
    # retorna el nombre introduït
    pass    

# MAIN
# Bucle principal del programa que va cridant les funcions

# mostrar el menú, de 1 a 6:
# 1: Llistar items
# 2: Afegir item
# 3: Treure item
# 4: Cercar item
# 5: Buildar llista
# 6: Sortir
# i executar la tasca indicada
