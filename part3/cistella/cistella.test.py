from cistella import *

def afegirUnCop():
    buidarItems()
    afegirItem('peres', 3)
    num = cercarItem('peres')
    assert num == 3

def afegirDosCops():
    buidarItems()
    afegirItem('peres', 1)
    afegirItem('peres', 2)
    num = cercarItem('peres')
    assert num == 3

def afegirDosItems():
    buidarItems()
    afegirItem('peres', 2)
    afegirItem('pomes', 3)
    num = cercarItem('peres')
    assert num == 2
    num = cercarItem('pomes')
    assert num == 3

def esborrarUnitats():
    buidarItems()
    afegirItem('peres', 3)
    ok = treureItem('peres', 1)
    assert ok
    num = cercarItem('peres')
    assert num == 2

def esborrarNoExisteix():
    buidarItems()
    ok = treureItem('peres', 1)
    assert not ok
    num = cercarItem('peres')
    assert num == 0

def esborrarNoSuficient():
    buidarItems()
    afegirItem('peres', 1)
    ok = treureItem('peres', 2)
    assert not ok
    num = cercarItem('peres')
    assert num == 1

def esborrarExacte():
    buidarItems()
    afegirItem('peres', 2)
    ok = treureItem('peres', 2)
    assert ok
    num = cercarItem('peres')
    assert num == 0

if __name__ == "__main__":
    afegirUnCop()
    afegirDosCops()
    afegirDosItems()
    esborrarUnitats()
    esborrarNoExisteix()
    esborrarNoSuficient()
    esborrarExacte()
    print("tests passed")
