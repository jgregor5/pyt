from mymodule import readInt
# import all using:
# from mymodule import *

from utils.submodule import printInt

nombre = None
while nombre is None:
  nombre = readInt("nombre", 5, 10)  
  if nombre is None:
    print("not between 5 and 10")

printInt(nombre)
