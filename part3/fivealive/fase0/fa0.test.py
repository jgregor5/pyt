from fa0 import *


def test():

  pnoms = ['Rosa', 'Pere']
  pmans = [[2, 0], [1, 1]]
  pjugada = [8, 6, 4]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)

  assert numJugadors() == 2
  assert punts() == 18
  assert jugat() == [8, 6, 4]
  assert nom(0) == 'Rosa'
  assert nom(1) == 'Pere'
  assert ma(0) == [2, 0]
  assert ma(1) == [1, 1]
  assert vida(0) == 1
  assert vida(1) == 2


if __name__ == "__main__":
  test()
  print("tests passed")
