
# GLOBALS

# noms: llista de strings amb els noms dels jugadors
# mans: llista de llista de sencers amb les mans
# jugada: llista de sencers amb les cartes a la taula
# vides: llista de sencers amb les vides
# guanyador: sencer amb el guanyador de la partida, -1 si no hi ha


# FUNCIONS

def inicia(pnoms, pmans, pjugada, pvides):
  global noms, mans, jugada, vides
  noms = pnoms
  mans = pmans
  jugada = pjugada
  vides = pvides


def numJugadors():
  """retorna el nombre total de jugadors"""
  return 0  # COMPLETAR


def jugat():
  """retorna una llista de sencers amb la jugada actual"""
  return []  # COMPLETAR


def ma(jugador):
  """retorna una llista de sencers amb les cartes d'un jugador (0 a N)"""
  return []  # COMPLETAR


def vida(jugador):
  """retorna un sencer amb les vides que li queden a un jugador (0 a N)"""
  return 0  # COMPLETAR


def nom(jugador):
  """retorna el nom d'un jugador (0 a N)"""
  return None  # COMPLETAR


def punts():
  """retorna els punts totals de les cartes jugades"""
  return 0  # COMPLETAR


def lstr(l):
  """retorna un string amb els valors sencers d'una llista"""
  return " ".join(map(str, l))


def veureTauler():
  """mostra el tauler"""
  print(f"\njugada: [{lstr(jugat())}] = {punts()}")
  for i in range(numJugadors()):
    print(f"{nom(i)} ({vida(i)}) [{lstr(ma(i))}]")


# MAIN


def main():
  pnoms = ['Rosa', 'Pere']
  pmans = [[2, 0], [1, 1]]
  pjugada = [8, 6, 4]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)
  veureTauler()


if __name__ == "__main__":
  main()
