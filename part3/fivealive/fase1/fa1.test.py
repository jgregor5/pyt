from fa1 import *


def test():
  pnoms = ['Rosa', 'Pere']
  pmans = [[2, 0], [1, 1]]
  pjugada = [8, 6, 4]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)

  assert punts() == 18
  assert nom(0) == 'Rosa'
  assert nom(1) == 'Pere'
  assert ma(0) == [2, 0]
  assert ma(1) == [1, 1]
  assert vida(0) == 1
  assert vida(1) == 2
  assert quiHaGuanyat() == -1

  assert jugar(0, 2) == True
  assert punts() == 20
  assert ma(0) == [0]
  assert ma(1) == [1, 1]
  assert vida(0) == 1
  assert vida(1) == 2
  assert jugat() == [8, 6, 4, 2]
  assert quiHaGuanyat() == -1

  assert jugar(1, 1) == True
  assert punts() == 21
  assert ma(0) == [0]
  assert ma(1) == [1]
  assert vida(0) == 1
  assert vida(1) == 2
  assert jugat() == [8, 6, 4, 2, 1]
  assert quiHaGuanyat() == -1

  assert jugar(0, 0) == True
  assert punts() == 21
  assert ma(0) == []
  assert ma(1) == [1]
  assert vida(0) == 1
  assert vida(1) == 1
  assert jugat() == [8, 6, 4, 2, 1, 0]
  assert quiHaGuanyat() == 0

  veureTauler()


def testalt():
  pnoms = ['Rosa', 'Pere']
  pmans = [[4, 0], [1, 1]]
  pjugada = [8, 6, 4]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)

  assert jugar(0, 4) == False
  assert jugar(0, 1) == False


if __name__ == "__main__":
  test()
  testalt()
  print("tests passed")
