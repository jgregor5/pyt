
# GLOBALS

# noms: llista de strings amb els noms dels jugadors
# mans: llista de llista de sencers amb les mans
# jugada: llista de sencers amb les cartes a la taula
# vides: llista de sencers amb les vides
# guanyador: sencer amb el guanyador de la partida, -1 si no hi ha


# FUNCIONS

def inicia(pnoms, pmans, pjugada, pvides):
  global noms, mans, jugada, vides
  noms = pnoms
  mans = pmans
  jugada = pjugada
  vides = pvides


def lstr(l):
  return " ".join(map(str, l))


def numJugadors():
  return len(vides)


def jugat():
  return jugada


def ma(jugador):
  return mans[jugador]


def vida(jugador):
  return vides[jugador]


def nom(jugador):
  return noms[jugador]


def veureTauler():
  print(f"\njugada: [{lstr(jugat())}] = {punts()}")
  for i in range(numJugadors()):
    print(f"{nom(i)} ({vida(i)}) [{lstr(ma(i))}]")


def punts():
  total = 0
  for p in jugada:
    total += p
  return total

# FASE 1


def jugar(jugador, carta):
  return False  # COMPLETAR


def quiHaGuanyat():
  return -1  # COMPLETAR

# MAIN


def main():
  pnoms = ['Rosa', 'Pere']
  pmans = [[2, 0], [1, 1]]
  pjugada = [8, 6, 4]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)
  veureTauler()
  jugar(0, 2)
  veureTauler()
  jugar(1, 1)
  veureTauler()
  jugar(0, 0)
  veureTauler()
  nomGuanyador = nom(quiHaGuanyat())
  print(f'guanyador és {nomGuanyador}')


if __name__ == "__main__":
  main()
