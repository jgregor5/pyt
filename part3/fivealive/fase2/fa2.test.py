from fa2 import *


def testpassa():
  pnoms = ['Rosa', 'Pere']
  pmans = [[2, 0], [1, 1]]
  pjugada = [8, 6, 4, 3]
  pvides = [1, 2]
  inicia(pnoms, pmans, pjugada, pvides)

  passar(1)
  assert ma(0) == [2, 0]
  assert ma(1) == [1, 1]
  assert vida(0) == 1
  assert vida(1) == 1
  assert jugat() == []
  assert quiHaGuanyat() == -1

  veureTauler()


def testtorn1():
  pnoms = ['Rosa', 'Pere', 'Joan']
  pmans = [[2, 0], [1, 1], []]
  pjugada = [8, 6, 4]
  pvides = [1, 2, 3]
  inicia(pnoms, pmans, pjugada, pvides)

  torn = seguentTorn(1)
  assert torn == 2
  torn = seguentTorn(2)
  assert torn == 0
  torn = seguentTorn(0)
  assert torn == 1


def testtorn2():
  pnoms = ['Rosa', 'Pere', 'Joan']
  pmans = [[2, 0], [1, 1], []]
  pjugada = [8, 6, 4]
  pvides = [1, 2, 0]  # Joan eliminat
  inicia(pnoms, pmans, pjugada, pvides)

  torn = seguentTorn(1)
  assert torn == 0
  torn = seguentTorn(0)
  assert torn == 1


def testviu1():
  pnoms = ['Rosa', 'Pere', 'Joan']
  pmans = [[2, 0], [1, 1], []]
  pjugada = [8, 6, 4]
  pvides = [1, 2, 0]
  inicia(pnoms, pmans, pjugada, pvides)

  assert unicViu() == -1


def testviu2():
  pnoms = ['Rosa', 'Pere', 'Joan']
  pmans = [[], [1, 1], []]
  pjugada = [8, 6, 4]
  pvides = [0, 2, 0]
  inicia(pnoms, pmans, pjugada, pvides)

  assert unicViu() == 1


def testiniciapartida():
  pnoms = ['Rosa', 'Pere', 'Joan']
  pmans = [[2, 0], [1, 1], []]
  pjugada = [8, 6, 4]
  pvides = [1, 2, 0]
  inicia(pnoms, pmans, pjugada, pvides, 1)

  assert quiHaGuanyat() == 1

  torn = iniciaPartida()
  assert torn == 0
  assert jugat() == []
  assert len(ma(0)) == 10
  assert len(ma(1)) == 10
  assert len(ma(2)) == 0
  assert quiHaGuanyat() == -1


if __name__ == "__main__":
  testpassa()
  testtorn1()
  testtorn2()
  testviu1()
  testviu2()
  testiniciapartida()
  print("tests passed")
