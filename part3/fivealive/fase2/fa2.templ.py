
# GLOBALS

from random import randrange, shuffle

# noms: llista de strings amb els noms
# mans: llista de llista de sencers amb les mans
# jugada: llista de sencers amb les cartes a la taula
# vides: llista de sencers amb les vides
# guanyador: qui ha guanyat, -1 si encara no ha acabat la partida

# FUNCIONS


def inicia(pnoms, pmans, pjugada, pvides, pguanyador=-1):
  global noms, mans, jugada, vides, guanyador
  guanyador = pguanyador
  noms = pnoms
  mans = pmans
  jugada = pjugada
  vides = pvides


def lstr(l):
  return " ".join(map(str, l))


def numJugadors():
  return len(vides)


def jugat():
  return jugada


def ma(jugador):
  return mans[jugador]


def vida(jugador):
  return vides[jugador]


def nom(jugador):
  return noms[jugador]


def veureTauler():
  print(f"\njugada: [{lstr(jugat())}] = {punts()}")
  for i in range(numJugadors()):
    print(f"{nom(i)} ({vida(i)}) [{lstr(ma(i))}]")


def punts():
  total = 0
  for p in jugada:
    total += p
  return total


def quiHaGuanyat():
  return guanyador


# FASE 1

def jugar(jugador, carta):
  if not carta in mans[jugador]:
    print(f"carta {carta} inexistent per {noms[jugador]}")
    return False
  elif punts() + carta > 21:
    print(f"carta {carta} fa superar 21")
    return False
  else:
    mans[jugador].remove(carta)
    jugada.append(carta)
    if len(mans[jugador]) == 0:
      haGuanyat(jugador)
      treureVidaAResta(jugador)
    return True


def haGuanyat(jugador):
  global guanyador
  guanyador = jugador
  print(f"{noms[jugador]} guanya")


def treureVidaAResta(jugador):
  for i in range(numJugadors()):
    if i != jugador and vides[i] > 0:
      treureVida(i)


def treureVida(jugador):
  vides[jugador] -= 1
  if vides[jugador] == 0:
    mans[jugador].clear()
    print(f"{noms[jugador]} eliminat")
  else:
    print(f"{noms[jugador]} perd")


# FASE 2

def jocNou():
  """Demana els jugadors i els seus noms i inicialitza totes les variables globals 
  (pots cridar la funció inicia per a assignar les variables globals)"""
  pass  # COMPLETAR


def iniciaPartida():
  """Inicia una partida, buidant la jugada, repartint les cartes i retornant el 
  jugador que té el primer torn"""
  return -1  # COMPLETAR


def inputJugada(jugador):
  """Pregunta la jugada per al jugador indicat, i accepta una carta a la mà o el 
  string "P" per a indicar que es passa (retorna un string amb la jugada)"""
  return None  # COMPLETAR


def passar(jugador):
  """Permet passar al jugador indicat, perdent una vida i buidant la jugada actual 
  (punts a zero)"""
  pass  # COMPLETAR


def seguentTorn(torn):
  """Calcula i retorna el següent torn, saltant els jugadors que han mort"""
  return None  # COMPLETAR


def unicViu():
  """Retorna l'únic jugador viu que queda o -1, si hi ha més d'un"""
  return -1  # COMPLETAR


# MAIN

def main():
  jocNou()
  while unicViu() == -1:
    torn = iniciaPartida()
    while quiHaGuanyat() == -1:
      veureTauler()
      carta = inputJugada(torn)
      if carta == 'P':
        passar(torn)
        torn = seguentTorn(torn)
      else:  # nombre
        if jugar(torn, int(carta)):
          torn = seguentTorn(torn)
  print(f"{nom(quiHaGuanyat())} guanyador final")


if __name__ == "__main__":
  main()
