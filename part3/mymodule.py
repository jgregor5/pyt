
def readInt(text, min = None, max = None):
  try:
    num = int(input(text + "? "))
    if min is not None and num < min:
      return None
    if max is not None and num > max:
      return None
    return num

  except ValueError:
    return None
