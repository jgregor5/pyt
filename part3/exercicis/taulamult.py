
def creaTaulaMult(nombre):
  nombres = []
  for i in range(1, 11):
    nombres.append(i * nombre)
  return nombres

# retorna True si num1 * num2 és resultat
def multEsCorrecte(num1, num2, resultat):
  return num1 * num2 == resultat

for i in range(1, 11):
  taula = creaTaulaMult(i)
  print(f"{i}: {taula}")

