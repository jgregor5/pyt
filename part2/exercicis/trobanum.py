import random

min = 1
max = 99
nombre = random.randrange(1, 100)
found = False
while not found:
    guess = int(input(f"entre {min} i {max}? "))
    if guess == nombre:
        found = True
    elif guess < nombre:
        print(f"més gran que {guess}")
        if guess >= min:
            min = guess+1
    elif guess > nombre:
        print(f"més petit que {guess}")
        if guess <= max:
            max = guess-1
print(f"correcte!")

# 4. Fes un programa que endevini un nombre que generes amb un random ("nombre").
# El programa demana que introdueixis un nombre ("entrada") entre A i B, inicialment 1 i 99.
# Si l'encertes, el felicites i acabes.
# Has d'anar canviant A i B segons el que introdueixi l'usuari per anar reduïnt l'interval.
