import random

errada = False
encerts = 0

while not errada:
  n1 = random.randrange(1,10)
  n2 = random.randrange(1,10)
  mult = int(input(f"{n1} per {n2}? "))
  if mult != n1 * n2:
    print(f"incorrecte, era {n1*n2}")
    errada = True
  else:
    encerts += 1

print(f"{encerts} encerts seguits!")
